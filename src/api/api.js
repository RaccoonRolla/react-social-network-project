import * as axios from 'axios';

const samuraiInstance = axios.create({
  withCredentials: true,
  baseURL: 'https://social-network.samuraijs.com/api/1.0/',
  headers: {
    'API-KEY': '6b4be270-36c2-463e-9c66-62423966358f',
  },
});

export const usersAPI = {
  getUsers(currentPage, pageSize) {
    return samuraiInstance.get(`users?page=${currentPage}&count=${pageSize}`);
  },
  followUser(userId) {
    return samuraiInstance.post(`follow/${userId}`);
  },
  unfollowUser(userId) {
    return samuraiInstance.delete(`follow/${userId}`);
  },
  getFriends(currentPage, pageSize) {
    return samuraiInstance.get(
      `users?page=${currentPage}&count=${pageSize}&friend=true`
    );
  },
};

export const authAPI = {
  authMe() {
    return samuraiInstance.get('auth/me');
  },
  login(loginData) {
    return samuraiInstance.post(`auth/login`, loginData);
  },
  logout() {
    return samuraiInstance.delete(`auth/login`);
  },
};

export const profileAPI = {
  getProfile(userId) {
    return samuraiInstance.get(`profile/${userId}`);
  },
  getStatus(userId) {
    return samuraiInstance.get(`profile/status/${userId}`);
  },
  updateStatus(statusText) {
    return samuraiInstance.put(`profile/status`, { status: statusText });
  },
};
