const initialState = {
  friends: [],
  pageSize: 10,
  totalUsersCount: 0,
  currentPage: 1,
  isFetching: true,
  isFollowingInProgress: [],
};

const friendsReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default friendsReducer;
