export const initialProfilePage = {
  posts: [
    { postText: 'Hi, this is my first post', likesCount: '10' },
    { postText: 'The second post', likesCount: '8' },
    { postText: 'Third post, dunno what to write here', likesCount: '20' },
    { postText: 'Fourth post, ok', likesCount: '15' },
  ],
  newPostText: '',
};

export const initialMessagesPage = {
  dialogs: [
    { id: 1, name: 'Alina' },
    { id: 2, name: 'Tim' },
    { id: 3, name: 'Kira' },
    { id: 4, name: 'Nikita' },
    { id: 5, name: 'Anton' },
    { id: 6, name: 'Nataliya' },
    { id: 7, name: 'Sasha' },
  ],
  messages: [
    {
      message: 'Hello, how are you?',
      userId: '1',
      className: 'messageReceiver',
    },
    {
      message: "Hi! I'm fine, thank you. And you?",
      userId: '2',
      className: 'messageSender',
    },
    {
      message: 'Where are you from?',
      userId: '1',
      className: 'messageReceiver',
    },
    { message: "I'm from Russia", userId: '2', className: 'messageSender' },
    {
      message: "I'm from Russia",
      userId: '1',
      className: 'messageReceiver',
    },
    { message: "I'm from Russia", userId: '2', className: 'messageSender' },
    {
      message: "I'm from Russia",
      userId: '1',
      className: 'messageReceiver',
    },
    { message: "I'm from Russia", userId: '2', className: 'messageSender' },
  ],
  newMessageText: '',
};

export const initialFriendsPage = {
  friends: [
    { id: 1, name: 'Alina' },
    { id: 2, name: 'Tim' },
    { id: 3, name: 'Kira' },
    { id: 4, name: 'Nikita' },
    { id: 5, name: 'Anton' },
    { id: 6, name: 'Nataliya' },
    { id: 7, name: 'Sasha' },
  ],
};
