import { authAPI } from '../api/api';

const SET_USER_DATA = 'SET_USER_DATA';
const CHECK_USER_AUTH = 'CHECK_USER_AUTH';
const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE';

const initialState = {
  id: null,
  login: null,
  email: null,
  isAuth: false,
  errorMessage: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return {
        ...state,
        ...action.payload,
      };
    case CHECK_USER_AUTH:
      return state.isAuth;
    case SET_ERROR_MESSAGE:
      return { ...state, errorMessage: action.errorMessage };
    default:
      return state;
  }
};

export const isUserAuth = () => ({
  type: CHECK_USER_AUTH,
});
export const setUserData = (id, login, email, isAuth) => ({
  type: SET_USER_DATA,
  payload: { id, login, email, isAuth },
});
export const setErrorMessage = (errorMessage) => ({
  type: SET_ERROR_MESSAGE,
  errorMessage,
});

export const authMe = () => (dispatch) => {
  authAPI.authMe().then((response) => {
    if (response.data.resultCode === 0) {
      const { id, login, email } = response.data.data;
      dispatch(setUserData(id, login, email, true));
    }
  });
};

export const login = (loginData) => (dispatch) => {
  authAPI.login(loginData).then((response) => {
    if (response.data.resultCode === 0) {
      dispatch(authMe());
    } else {
      response.data.messages.length !== 0
        ? dispatch(setErrorMessage(response.data.messages[0]))
        : dispatch(setErrorMessage('Some error occured'));
    }
  });
};

export const logout = () => (dispatch) => {
  authAPI.logout().then((response) => {
    if (response.data.resultCode === 0) {
      dispatch(setUserData(null, null, null, false));
      dispatch(setErrorMessage(null));
    }
  });
};

export default authReducer;
