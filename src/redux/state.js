import profileReducer from './profile-reducer';
import messagesReducer from './messages-reducer';
import friendsReducer from './friends-reducer';
import {
  initialProfilePage,
  initialMessagesPage,
  initialFriendsPage,
} from './__data__/initialState';

const store = {
  state: {
    profilePage: initialProfilePage,
    messagesPage: initialMessagesPage,
    friendsPage: initialFriendsPage,
  },

  _callSubscriber() {
    console.log('No subscriber (state)');
  },

  setState(state) {
    this.state = state;
  },

  getState() {
    return this.state;
  },

  subscribe(observer) {
    this._callSubscriber = observer;
  },

  dispatch(action) {
    this.state.profilePage = profileReducer(this._state.profilePage, action);
    this.state.messagesPage = messagesReducer(this.state.messagesPage, action);
    this.state.friendsPage = friendsReducer(this._state.friendsPage, action);
    this._callSubscriber(this._state);
  },
};

export default store;
