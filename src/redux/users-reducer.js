import { usersAPI } from '../api/api';

const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const SET_USERS = 'SET_USERS';
const SET_TOTAL_USERS_COUNT = 'SET_TOTAL_USERS_COUNT';
const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';
const SET_IS_FETCHING = 'SET_IS_FETCHING';
const TOGGLE_FOLLOWING_IN_PROGRESS = 'TOGGLE_FOLLOWING_IN_PROGRESS';

const initialState = {
  users: [],
  pageSize: 10,
  totalUsersCount: 0,
  currentPage: 1,
  isFetching: true,
  isFollowingInProgress: [],
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case FOLLOW:
      return {
        ...state,
        users: state.users.map((u) => {
          if (u.id === action.userId) {
            return { ...u, followed: true };
          }
          return u;
        }),
      };
    case UNFOLLOW:
      return {
        ...state,
        users: state.users.map((u) => {
          if (u.id === action.userId) {
            return { ...u, followed: false };
          }
          return u;
        }),
      };
    case SET_USERS:
      return { ...state, users: action.users };
    case SET_TOTAL_USERS_COUNT:
      return { ...state, totalUsersCount: action.totalUsersNumber };
    case SET_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.currentPageNumber,
      };
    case SET_IS_FETCHING:
      return {
        ...state,
        isFetching: action.isFetching,
      };
    case TOGGLE_FOLLOWING_IN_PROGRESS:
      return {
        ...state,
        isFollowingInProgress: action.isFetching
          ? [...state.isFollowingInProgress, action.userId]
          : state.isFollowingInProgress.filter((id) => id !== action.userId),
      };
    default:
      return state;
  }
};

export const follow = (userId) => ({ type: FOLLOW, userId });
export const unFollow = (userId) => ({ type: UNFOLLOW, userId });
export const setUsers = (users) => ({ type: SET_USERS, users });
export const setTotalUsersCount = (totalUsersNumber) => ({
  type: SET_TOTAL_USERS_COUNT,
  totalUsersNumber,
});
export const setCurrentPage = (currentPageNumber) => ({
  type: SET_CURRENT_PAGE,
  currentPageNumber,
});
export const setIsFetching = (isFetching) => ({
  type: SET_IS_FETCHING,
  isFetching,
});

export const toggleFollowingInProgress = (isFetching, userId) => ({
  type: TOGGLE_FOLLOWING_IN_PROGRESS,
  isFetching,
  userId,
});

export const getUsers = (currentPage, pageSize) => {
  return (dispatch) => {
    usersAPI.getUsers(currentPage, pageSize).then((response) => {
      dispatch(setUsers(response.data.items));
      dispatch(setTotalUsersCount(response.data.totalCount));
      dispatch(setIsFetching(false));
    });
  };
};
export const unfollowUser = (userId) => (dispatch) => {
  dispatch(toggleFollowingInProgress(true, userId));
  usersAPI.unfollowUser(userId).then((response) => {
    if (response.data.resultCode === 0) {
      dispatch(unFollow(userId));
    }
    dispatch(toggleFollowingInProgress(false, userId));
  });
};
export const followUser = (userId) => (dispatch) => {
  dispatch(toggleFollowingInProgress(true, userId));
  usersAPI.followUser(userId).then((response) => {
    if (response.data.resultCode === 0) {
      dispatch(follow(userId));
    }
    dispatch(toggleFollowingInProgress(false, userId));
  });
};
export const getFriends = (currentPage, pageSize) => {
  return (dispatch) => {
    usersAPI.getFriends(currentPage, pageSize).then((response) => {
      dispatch(setUsers(response.data.items));
      dispatch(setTotalUsersCount(response.data.totalCount));
      dispatch(setIsFetching(false));
    });
  };
};

export default usersReducer;
