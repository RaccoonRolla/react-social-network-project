import React from 'react';
import { Route } from 'react-router-dom';
import { hot } from 'react-hot-loader/root';

import ErrorBoundary from './ErrorBoundary';
import ProfileContainer from '../../pages/ProfileContainer';
import UsersContainer from '../../pages/UsersContainer';
import MessengerContainer from '../../pages/MessengerContainer';
import Feed from '../../pages/Feed';
import Music from '../../pages/Music';
import Settings from '../../pages/Settings';
import Login from '../../pages/Login';

// todo для работы с маршрутизацией предлагаю поискать/запилить самому "динамический роутер".
//  Ищи информацию о lazy, Suspense методах библиотеки react
const Content = () => {
  return (
    <div className="content">
      {/*<Switch>
        {routes.map(({ page: Page, ...route }) => (
          <Route
            key={route.path}
            render={({ staticContext, match, status }) => {
              // todo это не обязательно, можно пренебречь
              if (status && staticContext) {
                staticContext.status = status;
                staticContext.match = match;
              }
              return <Page />;
            }}
            {...route}
          />
        ))}
      </Switch>*/}

      <Route
        path="/profile/:userId"
        render={() => (
          <ErrorBoundary>
            <ProfileContainer />
          </ErrorBoundary>
        )}
      />
      <Route
        path="/friends"
        render={() => (
          <ErrorBoundary>
            <UsersContainer />
          </ErrorBoundary>
        )}
      />
      <Route
        path="/messenger"
        render={() => (
          <ErrorBoundary>
            <MessengerContainer />
          </ErrorBoundary>
        )}
      />
      <Route
        path="/feed"
        render={() => (
          <ErrorBoundary>
            <Feed />
          </ErrorBoundary>
        )}
      />
      <Route
        path="/music"
        render={() => (
          <ErrorBoundary>
            <Music />
          </ErrorBoundary>
        )}
      />
      <Route
        path="/users"
        render={() => (
          <ErrorBoundary>
            <UsersContainer />
          </ErrorBoundary>
        )}
      />
      <Route
        path="/settings"
        render={() => (
          <ErrorBoundary>
            <Settings />
          </ErrorBoundary>
        )}
      />
      <Route
        path="/login"
        render={() => (
          <ErrorBoundary>
            <Login />
          </ErrorBoundary>
        )}
      />
    </div>
  );
};

export default hot(Content);
