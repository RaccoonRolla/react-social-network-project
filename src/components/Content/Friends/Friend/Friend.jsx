import React from 'react';

import Button from '@material-ui/core/Button';
import style from './Friend.module.css';

const Friend = (props) => {
  return (
    <div className={style.friendIcon}>
      <img
        src={'https://www.shareicon.net/data/256x256/2016/05/30/772931_users_512x512.png'}
        alt={'avatar'}
        className={style.friendAvatar}
      />
      <div>{props.name}</div>
      <Button variant="contained">Unfollow</Button>
    </div>
  );
};

export default Friend;
