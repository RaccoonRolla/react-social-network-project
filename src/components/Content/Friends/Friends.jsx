import React from 'react';

import Users from '../FindUsers/Users';

const Friends = (props) => {
  return <Users {...props} />;
};

export default Friends;
