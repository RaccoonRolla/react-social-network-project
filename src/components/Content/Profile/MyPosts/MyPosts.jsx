import React from 'react';

import Post from './Post/Post';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const MyPosts = (props) => {
  const mapPostsData = props.posts.map((post, index) => (
    <Post key={props.id + '_' + index} postText={post.postText} likeCount={post.likesCount} />
  ));

  const newPost = React.createRef();

  const onAddPost = () => {
    props.addPost();
  };

  const onUpdateNewPostText = () => {
    const newPostText = newPost.current.value;
    props.updateNewPostText(newPostText);
  };

  const onEnterPress = (e) => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      e.preventDefault();
      onAddPost();
    }
  };

  return (
    <div>
      <h1>My posts</h1>
      <div>
        <div>
          <TextField
            id="standard-multiline-static"
            variant="outlined"
            multiline
            rows={4}
            label="New post"
            onChange={onUpdateNewPostText}
            inputRef={newPost}
            value={props.newPostText}
            onKeyDown={onEnterPress}
          />
        </div>
        <div>
          <Button variant="contained" color="primary" onClick={onAddPost}>
            Add post
          </Button>
        </div>
      </div>
      <br />
      <div>{mapPostsData}</div>
    </div>
  );
};

export default MyPosts;
