import { connect } from 'react-redux';

import MyPosts from './MyPosts';

import { addPost, updateNewPostText } from '../../../../redux/profile-reducer';

const mapStateToProps = (state) => {
  return {
    posts: state.profilePage.posts,
    newPostText: state.profilePage.newPostText,
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     addPost: () => dispatch(addPostAC()),
//     updateNewPostText: (newPostText) => dispatch(updateNewPostTextAC(newPostText)),
//   };
// };

// todo смоделируй ситуацию, когда получаешь свои посты через API.
//  Если пока непонятно как это сделать касательно процессов запрос-ответ,
//  то реализуй хотя бы каким-то setTimeout экшеном

const MyPostsContainer = connect(mapStateToProps, {
  addPost,
  updateNewPostText,
})(MyPosts);

export default MyPostsContainer;
