import React, { useState } from 'react';

const ProfileStatus = (props) => {
  const [editMode, setEditMode] = useState(false);
  const [status, setStatus] = useState(props.status);

  const onUpdateStatusText = (e) => {
    setStatus(e.currentTarget.value);
  };
  const updateStatus = () => {
    props.updateStatus(status);
  };
  const onEnterPress = (e) => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      e.preventDefault();
      setEditMode(false);
      updateStatus(status);
    }
  };

  return (
    <div>
      {!editMode && (
        <div>
          <span onDoubleClick={() => setEditMode(true)}>
            {props.status === '' || props.status === null
              ? 'No status'
              : props.status}
          </span>
        </div>
      )}
      {editMode && (
        <div>
          <input
            autoFocus={true}
            onBlur={() => {
              setEditMode(false);
              updateStatus(status);
            }}
            onChange={onUpdateStatusText}
            onKeyDown={onEnterPress}
            value={status || props.status}
          />
        </div>
      )}
    </div>
  );
};

export default ProfileStatus;
