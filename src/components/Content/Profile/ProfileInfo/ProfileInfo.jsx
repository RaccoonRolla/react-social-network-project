import React from 'react';

import ProfileStatus from './ProfileStatus';

import style from './ProfileInfo.module.css';
import userPhoto from './../../../../media/user_no_photo.png';

const ProfileInfo = (props) => {
  return (
    <div className={style.profile}>
      <div>
        <h3>{props.profile.fullName}</h3>
        <div>
          <img
            src={
              props.profile.photos.large != null
                ? props.profile.photos.large
                : userPhoto
            }
            alt="ava"
          />
          <ProfileStatus {...props} />
        </div>
        <div>
          <div>Description</div>
        </div>
      </div>
    </div>
  );
};

export default ProfileInfo;
