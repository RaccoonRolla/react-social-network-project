import React from 'react';

import ProfileInfo from './ProfileInfo/ProfileInfo';
import MyPostsContainer from './MyPosts/MyPostsContainer';
import Preloader from '../../../media/Preloader/Preloader';

import style from './Profile.module.css';

const Profile = (props) => {
  if (!props.profile) {
    return <Preloader />;
  }
  return (
    <div className={style.profile}>
      <ProfileInfo {...props} profile={props.profile} />
      <MyPostsContainer />
    </div>
  );
};

export default Profile;
