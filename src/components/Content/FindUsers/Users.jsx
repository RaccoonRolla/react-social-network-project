import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import Pagination from '@material-ui/lab/Pagination';
import style from './Users.module.css';
import userPhoto from './../../../media/user_no_photo.png';
import Preloader from '../../../media/Preloader/Preloader';

const Users = ({
  currentPage,
  totalUsersCount,
  getUsers,
  pageSize,
  getFriends,
  setIsFetching,
  ...props
}) => {
  // todo для такого рода вещей предлагаю почитать что такое redux-saga и redux-thunk

  useEffect(() => {
    if (window.location.pathname === '/users') {
      getUsers(currentPage, pageSize);
    } else if (window.location.pathname === '/friends') {
      getFriends(currentPage, pageSize);
    }
    return setIsFetching(true);
  }, [getUsers, getFriends, currentPage, pageSize, setIsFetching]);

  const pageChangeHandler = (event, value) => {
    props.setCurrentPage(value);
    setIsFetching(true);
  };

  const pagesCount = Math.ceil(totalUsersCount / pageSize);

  return (
    <div>
      {window.location.pathname === '/users' && <h3>Users</h3>}
      {window.location.pathname === '/friends' && <h3>Friends</h3>}
      <Pagination
        count={pagesCount}
        color="secondary"
        page={props.currentPage}
        onChange={pageChangeHandler}
      />
      {props.isFetching ? (
        <Preloader />
      ) : (
        <>
          <div>
            {props.users.map((u, index) => (
              <div key={u.id + '_' + index} className={style.container}>
                <div className={style.avatar}>
                  <Link to={`/profile/${u.id}`}>
                    <img
                      src={u.photos.large != null ? u.photos.large : userPhoto}
                      alt="user avatar"
                    />
                  </Link>
                  {u.followed ? (
                    <Button
                      variant="contained"
                      disabled={props.isFollowingInProgress.some(
                        (id) => id === u.id
                      )}
                      onClick={() => {
                        props.unfollowUser(u.id);
                      }}
                    >
                      Unfollow
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      color="secondary"
                      disabled={props.isFollowingInProgress.some(
                        (id) => id === u.id
                      )}
                      onClick={() => {
                        props.followUser(u.id);
                      }}
                    >
                      Follow
                    </Button>
                  )}
                </div>
                <div className={style.userCard}>
                  <div>
                    <h4>{u.name}</h4>
                    <div>{u.status ? u.status : 'No status'}</div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </>
      )}
    </div>
  );
};

export default Users;
