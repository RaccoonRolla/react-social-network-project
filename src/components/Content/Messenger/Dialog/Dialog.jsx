import React from 'react';
import { NavLink } from 'react-router-dom';

import style from './Dialog.module.css';

const Dialog = (props) => {
  const path = '/messenger/' + props.id;
  return (
    <div className={style.dialog}>
      <NavLink to={path} activeClassName={style.active}>
        {props.name}
      </NavLink>
    </div>
  );
};

export default Dialog;
