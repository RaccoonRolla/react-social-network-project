import React from 'react';

const Message = (props) => {
  return (
    <li className={props.className}>
      <img
        src="https://www.shareicon.net/data/256x256/2016/05/30/772931_users_512x512.png"
        alt={'avatar'}
        id={props.id}
      />
      {props.message}
    </li>
  );
};

export default Message;
