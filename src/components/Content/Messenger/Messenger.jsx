import React from 'react';

import Dialog from './Dialog/Dialog';
import Message from './Message/Message';

import Button from '@material-ui/core/Button';
import style from './Messenger.module.css';

const Messenger = (props) => {
  const mapMessages = props.messages.map((message, index) => (
    <Message
      message={message.message}
      id={message.userId}
      className={style[message.className]}
      key={message.userId + '' + index}
    />
  ));

  const mapDialogElements = props.dialogs.map((dialog) => (
    <Dialog
      name={dialog.name}
      id={dialog.id}
      key={dialog.name + '_' + dialog.id}
    />
  ));

  const newMessage = React.createRef();

  const onSendMessage = () => {
    props.sendMessage();
  };

  const onUpdateNewMessageText = () => {
    const newMessageText = newMessage.current.value;
    props.updateNewMessageText(newMessageText);
  };

  const onEnterPress = (e) => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      e.preventDefault();
      onSendMessage();
    }
  };
  return (
    <div className={style.dialogs}>
      <div className={style.dialogsList}>{mapDialogElements}</div>
      <div className={style.messengerContainer}>
        <ol className={style.messenger}>{mapMessages}</ol>
      </div>

      <div></div>
      <textarea
        className={style.messengerTextArea}
        onChange={onUpdateNewMessageText}
        value={props.newMessageText}
        placeholder={'Type your message here'}
        ref={newMessage}
        onKeyDown={onEnterPress}
      />
      <div></div>
      <Button
        variant="contained"
        color="primary"
        className={style.messengerButton}
        onClick={onSendMessage}
      >
        Send message
      </Button>
    </div>
  );
};

export default Messenger;
