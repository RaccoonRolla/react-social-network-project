import React from 'react';
import { NavLink } from 'react-router-dom';

import style from './Header.module.css';
import { Button } from '@material-ui/core';

// todo обычно используешь <a></a>, когда нужна внешняя ссылка,
//  иначе есть NavLink
const Header = (props) => {
  const logout = () => {
    props.logout();
  };
  return (
    <header className={style.header}>
      <NavLink to="/profile">
        <img
          src="https://img.icons8.com/bubbles/2x/facebook-new.png"
          alt="avatar"
          className={style.headerLogo}
        />
      </NavLink>
      <div className={style.loginBlock}>
        {props.isAuth ? (
          <div className={style.loggedIn}>
            <div className={style.login}>{props.login}</div>
            <div>
              <Button variant="contained" onClick={logout}>
                Logout
              </Button>
            </div>
          </div>
        ) : (
          <Button variant="contained" color="secondary">
            <NavLink to="/login">Login</NavLink>
          </Button>
        )}
      </div>
    </header>
  );
};

export default Header;
