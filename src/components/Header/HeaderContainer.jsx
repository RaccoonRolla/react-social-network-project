import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import Header from './Header';
import { authMe, logout, setUserData } from '../../redux/auth-reducer';

const HeaderContainer = ({ setUserData, authMe, ...props }) => {
  useEffect(() => {
    authMe();
  }, [authMe]);
  return <Header {...props} />;
};

const mapStateToProps = (state) => ({
  login: state.auth.login,
  isAuth: state.auth.isAuth,
});
export default connect(mapStateToProps, { setUserData, authMe, logout })(
  HeaderContainer
);
