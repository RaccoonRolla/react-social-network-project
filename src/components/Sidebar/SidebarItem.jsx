import React from 'react';
import { NavLink } from 'react-router-dom';

import style from './Sidebar.module.css';

const SidebarItem = (props) => {
  return (
    <div className={style.item}>
      <NavLink to={props.path} activeClassName={style.active}>
        {props.name}
      </NavLink>
    </div>
  );
};

export default SidebarItem;
