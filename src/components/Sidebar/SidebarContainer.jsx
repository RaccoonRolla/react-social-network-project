import React from 'react';

import Sidebar from './Sidebar';
import { connect } from 'react-redux';

const SidebarContainer = (props) => {
  return <Sidebar {...props} />;
};

const mapStateToProps = (state) => ({
  id: state.auth.id,
});

export default connect(mapStateToProps)(SidebarContainer);
