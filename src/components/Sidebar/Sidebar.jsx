import React from 'react';

import SidebarItem from './SidebarItem';

import style from './Sidebar.module.css';

const Sidebar = (props) => {
  return (
    <nav className={style.sidebar}>
      <SidebarItem name="Profile" path={`/profile/${props.id}`} />
      <SidebarItem name="Friends" path={'/friends'} />
      <SidebarItem name="Messenger" path={'/messenger'} />
      <SidebarItem name="Feed" path={'/feed'} />
      <SidebarItem name="Music" path={'/music'} />
      <br />
      <SidebarItem name="Find users" path={'/users'} />
      <br />
      <SidebarItem name="Settings" path={'/settings'} />
    </nav>
  );
};

export default Sidebar;
