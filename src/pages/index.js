import React from 'react';
import loadable from '@loadable/component';

import Preloader from '../media/Preloader/Preloader';
import { isUserAuth } from '../redux/auth-reducer';

const AsyncPage = loadable((props) => import(`./${props.page}`), {
  fallback: <Preloader />,
});

const routes = isUserAuth()
  ? [
      {
        path: '/profile/:userId',
        page: 'ProfileContainer',
        exact: true,
      },
      /*{
        path: '/friends',
        page: 'UsersContainer',
        exact: true,
      },*/
      {
        path: '/messenger',
        page: 'MessengerContainer',
        exact: false,
      },
      {
        path: '/feed',
        page: 'Feed',
        exact: true,
      },
      {
        path: '/music',
        page: 'Music',
        exact: true,
      },
      {
        path: '/users',
        page: 'UsersContainer',
        exact: false,
      },
      {
        path: '/settings',
        page: 'Settings',
        exact: true,
      },
    ]
  : [
      {
        path: '/login',
        page: 'Login',
        exact: true,
      },
    ];

const finalRoutes = routes.map((route) => ({
  ...route,
  exact: !!route.exact,
  page: () => <AsyncPage page={route.page} path={route.path} />,
}));

export default finalRoutes;
