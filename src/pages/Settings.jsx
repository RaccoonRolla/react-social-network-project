import React from 'react';
import { withAuthRedirect } from '../hoc/withAuthRedirect';

const Settings = () => {
  return <div>Some settings</div>;
};

export default withAuthRedirect(Settings);
