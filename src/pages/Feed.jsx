import React from 'react';
import { withAuthRedirect } from '../hoc/withAuthRedirect';

const Feed = () => {
  return <div>Here will be your news feed</div>;
};

export default withAuthRedirect(Feed);
