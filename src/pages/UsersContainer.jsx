import { connect } from 'react-redux';

import Users from '../components/Content/FindUsers/Users';
import {
  follow,
  setUsers,
  unFollow,
  setTotalUsersCount,
  setCurrentPage,
  toggleFollowingInProgress,
  setIsFetching,
  getUsers,
  unfollowUser,
  followUser,
  getFriends,
} from '../redux/users-reducer';
import { withAuthRedirect } from '../hoc/withAuthRedirect';
import { compose } from 'redux';

const mapStateToProps = (state) => {
  return {
    users: state.usersPage.users,
    pageSize: state.usersPage.pageSize,
    totalUsersCount: state.usersPage.totalUsersCount,
    currentPage: state.usersPage.currentPage,
    isFetching: state.usersPage.isFetching,
    isFollowingInProgress: state.usersPage.isFollowingInProgress,
  };
};

export default compose(
  connect(mapStateToProps, {
    follow,
    setUsers,
    unFollow,
    setTotalUsersCount,
    setCurrentPage,
    setIsFetching,
    toggleFollowingInProgress,
    getUsers,
    unfollowUser,
    followUser,
    getFriends,
  }),
  withAuthRedirect
)(Users);
