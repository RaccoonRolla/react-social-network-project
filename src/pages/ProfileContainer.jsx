import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Profile from '../components/Content/Profile/Profile';
import {
  getStatus,
  getUserProfile,
  setUserProfile,
  updateStatus,
} from '../redux/profile-reducer';
import { withAuthRedirect } from '../hoc/withAuthRedirect';
import { compose } from 'redux';

const ProfileContainer = ({
  setUserProfile,
  getUserProfile,
  getStatus,
  ...props
}) => {
  const userId = props.match.params.userId;
  useEffect(() => {
    getUserProfile(userId);
    getStatus(userId);
    return setUserProfile(null);
  }, [getUserProfile, setUserProfile, userId, getStatus]);

  return <Profile {...props} />;
};

const mapStateToProps = (state) => ({
  profile: state.profilePage.profile,
  status: state.profilePage.status,
  newStatusText: state.profilePage.newStatusText,
});

export default compose(
  withAuthRedirect,
  withRouter,
  connect(mapStateToProps, {
    setUserProfile,
    getUserProfile,
    updateStatus,
    getStatus,
  })
)(ProfileContainer);
