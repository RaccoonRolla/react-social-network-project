import React from 'react';
import { withAuthRedirect } from '../hoc/withAuthRedirect';

const Music = () => {
  return (
    <>
      <iframe
        title="Spotify Music"
        src="https://open.spotify.com/embed/playlist/37i9dQZF1E4rXa759IrEnM"
        width="96%"
        height="98%"
        frameBorder="0"
        allowTransparency="true"
        allow="encrypted-media"
      ></iframe>
    </>
  );
};

export default withAuthRedirect(Music);
