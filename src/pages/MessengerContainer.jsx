import { connect } from 'react-redux';

import Messenger from '../components/Content/Messenger/Messenger';

import { sendMessage, updateNewMessageText } from '../redux/messages-reducer';
import { withAuthRedirect } from '../hoc/withAuthRedirect';
import { compose } from 'redux';

const mapStateToProps = (state) => {
  return {
    messages: state.messagesPage.messages,
    newMessageText: state.messagesPage.newMessageText,
    dialogs: state.messagesPage.dialogs,
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     sendMessage: () => dispatch(sendMessageAC()),
//     updateNewMessageText: (newMessageText) =>
//       dispatch(updateNewMessageTextAC(newMessageText)),
//   };
// };

export default compose(
  withAuthRedirect,
  connect(mapStateToProps, {
    sendMessage,
    updateNewMessageText,
  })
)(Messenger);
