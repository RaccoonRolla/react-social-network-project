import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import LoginForm from '../components/LoginForm/LoginForm';
import { login, setErrorMessage } from '../redux/auth-reducer';
import ModalMessage from '../media/ModalMessage/ModalMessage';

const Login = ({ login, isAuth, userId, errorMessage }) => {
  const onSubmit = (loginData) => {
    login(loginData);
  };

  if (isAuth) {
    return <Redirect to={`/profile/${userId}`} />;
  }
  return (
    <>
      <h3>Login</h3>
      <LoginForm onSubmit={onSubmit} />
      {errorMessage && <ModalMessage header={'Error'} message={errorMessage} />}
    </>
  );
};

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  userId: state.auth.id,
  errorMessage: state.auth.errorMessage,
});

export default connect(mapStateToProps, { login, setErrorMessage })(Login);
