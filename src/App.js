import React from 'react';

import SidebarContainer from './components/Sidebar/SidebarContainer';
import Content from './components/Content/Content';
import HeaderContainer from './components/Header/HeaderContainer';

import Container from '@material-ui/core/Container';
import './App.css';

const App = () => {
  return (
    <Container maxWidth="lg">
      <div className="App">
        <HeaderContainer />
        <SidebarContainer />
        <Content className="content" />
      </div>
    </Container>
  );
};

export default App;
